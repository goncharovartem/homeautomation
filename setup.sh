#!/bin/bash

virtualenv-3.5 server/venv
source server/venv/bin/activate
pip install -r server/requirements.txt
deactivate

cd web
npm install
cd -