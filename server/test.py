import unittest

from tests.test_base import GeneralAppTestCase
from tests.test_api import ApiTestCase


"""General entry point for the tests
"""
if __name__ == '__main__':
    unittest.main()
